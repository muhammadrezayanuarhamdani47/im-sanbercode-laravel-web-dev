<?php
    require_once("animal.php");
    require_once("Frog.php");
    require_once("Ape.php");
    
    $sheep = new Animal("shaun");
    echo "Name : " . $sheep->namahewan . "<br>"; // "shaun"
    echo "Legs : " . $sheep->legs . "<br>"; // 4
    echo "cold  blooded : " . $sheep->cold_blooded . "<br>" . "<br>"; // "no"

    $kodok = new Frog("buduk");
    echo "Name : " . $kodok->namahewan . "<br>"; // "buduk"
    echo "Legs : " . $kodok->legs . "<br>"; // 4
    echo "cold  blooded : " . $kodok->cold_blooded . "<br>"; // "no"
    echo "Jump : " . $kodok->jump();

    $sungokong = new Ape("kera sakti");
    echo "Name : " . $sungokong->namahewan . "<br>"; // "buduk"
    echo "Legs : " . $sungokong->legs . "<br>"; // 4
    echo "cold  blooded : " . $sungokong->cold_blooded . "<br>"; // "no"
    echo "Yell : " . $sungokong->yell();



?>
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Comments;
use Illuminate\Support\Facades\Auth;

class CommentsController extends Controller
{
    public function store($film_id, Request $request){
        $request->validate([
            'content'=>'required',
        ]);
        
        Comments::create([
            'user_id' => Auth::id(),
            'film_id' => $film_id ,
            'content' => $request->input('content')
        ]);

        return redirect('/film/'.$film_id);
    }
}

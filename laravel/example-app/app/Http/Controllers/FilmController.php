<?php

namespace App\Http\Controllers;

use App\models\Genre;
use App\models\Film;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use File;


class FilmController extends Controller

{
    public function create()
    {
        $genrelist = Genre::all();

        return view('film.create', ['genrelist'=> $genrelist]);
    }
 
    public function store(Request $request)
    {
     $request->validate([
         'judul'=>'required|min:5',
         'ringkasan'=>'required|min:20',
         'tahun'=>'required|min:4',
         'poster'=>'required|mimes:png,jpg,jpeg|max:2048',
         'genre_id'=>'required',
     ]);

     $imagename = time().'.'.$request->poster->extension();

     $request->poster->move(public_path('image'), $imagename);
     
     Film::create([
        'judul'=>$request->input("judul"),
        'ringkasan'=>$request->input("ringkasan"),
        'tahun'=>$request->input("tahun"),
        'poster'=>$imagename,
        'genre_id'=>$request->input("genre_id"),
     ]);
         return redirect('/film');
     }
 
 
     public function index()
     {
         $film = Film::all();
 
         return view('film.index', ['film' => $film]);
     }
 
     public function show($id)
     {
     $film = Film::find($id);
     return view('film.show', ['film' => $film]); 
     }
 
 public function edit($id){
     $film = Film::find($id);
     $genrelist = Genre::all();
     return view('film.edit', ['film' => $film, 'genrelist' => $genrelist]); 
 }
 
 
 
 public function update(Request $request, string $id){
     $request->validate([ 
         'judul'=>'required|min:5',
         'ringkasan'=>'required|min:20',
         'tahun'=>'required|min:4',
         'poster'=>'mimes:png,jpg,jpeg|max:2048',
         'genre_id'=>'required',     
     ]);
 
     $film= Film::find($id);

        if($request->has('poster')){
            File::delete('image/'.$film->poster);

            $imagename = time().'.'.$request->poster->extension();

            $request->poster->move(public_path('image'), $imagename);

            $film->poster= $imagename;
        }


        $film->judul = $request->input("judul");
        $film->ringkasan = $request->input("ringkasan");
        $film->tahun = $request->input("tahun");
        $film->genre_id = $request->input("genre_id");

        $film->save();

        return redirect('/film');
 
 }
 
 
 public function destroy($id){
    $film= Film::find($id);

    File::delete('image/'.$film->poster);

    $film->delete();
 
    return redirect('/film');
 }
}

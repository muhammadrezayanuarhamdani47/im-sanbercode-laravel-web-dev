<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Genre;


class GenreController extends Controller
{
   public function create()
   {
        return view('genre.create');
   }

   public function store(Request $request)
   {
    $request->validate([
        'nama'=>'required|min:5',
    ]);
    
    $query = DB::table('genre')->insert([
        'nama'=>$request["nama"]
    ]);
        return redirect('/genre');
    }


    public function index()
    {
        $genre = DB::table('genre')->get();

        return view('genre.index', ['genre' => $genre]);
    }

    public function show($id)
    {
    $genres = Genre::find($id);
    return view('genre.show', ['genres' => $genres]); 
    }

public function edit($id){
    $genres = DB::table('genre')->find($id);
    return view('genre.edit', ['genres' => $genres]); 
}

public function update($id, Request $request){
    $request->validate([ 
        'nama' => 'required|min:5',     
    ]);

    DB::table('genre')
          ->where('id', $id)
          ->update([
                     'nama' => $request->input('nama'),
                    ]);
            return redirect('/genre');

}


public function destroy($id){
    DB::table('genre')->where('id', '=', $id)->delete();

return redirect('/genre');
}
}
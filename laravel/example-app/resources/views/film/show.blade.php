@extends('layout.master')

@section('judul')
Film
@endsection
@section('content')

<img src="{{asset('image/'. $film->poster)}}" alt="">

<h1 class="text-primary">{{$film->judul}}</h1>
<p>{{$film->tahun}}</p>
<p>{{$film->ringkasan}}</p>

<hr>
<h4>List Ulasan</h4>
@forelse ($film->Comments as $item)
    <div class="card">
        <div class="card-header">
        {{$item->user->name}}
        </div>
        <div class="card-body">
        <p class="card-text">{{$item->content}}</p>
        </div>
    </div>
@empty
    <h4>Tidak ada ulasan di film ini</h4>
@endforelse

@auth
<hr>
<h4>Tambah Ulasan</h4>
<form action="/comments/{{$film->id}}" method="POST" class="mb-5">
    @csrf
    <textarea name="content" id="" cols="30" rows="10" class="form-control" placeholder="Isi Ulasan"></textarea> <br>
    <input type="submit" value="Beri Ulasan" class="btn btn-sm btn-block btn-primary">
</form>
@endauth

<a href="/film" class="btn btn-secondary btn-sm">Kembali</a>

@endsection
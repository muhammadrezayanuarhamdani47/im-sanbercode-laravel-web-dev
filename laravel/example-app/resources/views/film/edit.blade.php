@extends('layout.master')

@section('judul')
Edit film 
@endsection

@section('content')
<form action="/film/{{$film->id}}" method="POST" enctype="multipart/form-data">
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
 
<!-- Create Post Form -->
    @csrf
    @method('PUT')
    <div class="form-group">
        <label >Genre</label>
        <select name="genre_id" class="form-control" id="">
            <option value="">--Pilih Genre--</option>
            @forelse ($genrelist as $item)
                @if ($item->id === $film->genre_id)
                   <option value={{$item->id}} selected>{{$item->nama}}</option>
                    
                @else
                   <option value={{$item->id}}>{{$item->nama}}</option>
                @endif
            
            @empty
            <option value="">Tidak ada Genre</option>
            @endforelse
        </select>
    </div>
    <div class="form-group">
        <label >Judul</label>
        <input type="string" value="{{$film->judul}}" name="judul" class="form-control">
      </div>
      <div class="form-group">
        <label >Sinopsis</label>
        <textarea name="ringkasan" class="form-control" id="" cols="30" rows="10">{{$film->ringkasan}}</textarea>
      </div>
      <div class="form-group">
          <label >Tahun</label>
          <input type="number" value="{{$film->tahun}}" name="tahun" class="form-control">  
      </div>
      <div class="form-group">
          <label >Poster</label>
          <input type="file" name="poster" class="form-control">
      </div>
      <button type="submit" class="btn btn-primary">Update</button>
      </form>
      @endsection 
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sign Up</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <form action="/welcome" method="post">
    @csrf
    <h4>Sign Up Form</h4>
    <label>First name:</label> <br>
    <input type="text" name="fname"> <br> <br>
    <label>Last name:</label> <br>
    <input type="text" name="lname"> <br><br>
    <label>Gender:</label> <br> <br>
    <input type="radio" name="status"> Male <br>
    <input type="radio" name="status"> Female <br>
    <input type="radio" name="status"> Other <br> <br>
    <label>Nationality:</label> <br><br>
    <select name="kebangsaan">
        <option value="">Indonesian</option>
        <option value="">English</option>
        <option value="">Other</option>

    </select> <br><br>
    
    <label>Languange Spoken:</label> <br><br>
    <input type="checkbox" name="bahasa"> Bahasa Indonesia <br>
    <input type="checkbox" name="bahasa"> English <br>
    <input type="checkbox" name="bahasa"> Other <br> <br>
    <label>Bio:</label> <br><br>
    <textarea cols="30" rows="15"></textarea> <br>

    <input type="submit" value="Sign Up">
</form>
</body>
</html>
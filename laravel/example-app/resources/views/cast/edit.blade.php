@extends('layout.master')

@section('judul')
Halaman Edit Cast 
@endsection

@section('content')
<form action="/cast/{{$casters->id}}" method="POST">
    @method('put')
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
 
<!-- Create Post Form -->
    @csrf
    <div class="form-group">
      <label >Cast Name</label>
      <input type="text" value="{{$casters->nama}}" name="nama" class="form-control">
    </div>
    <div class="form-group">
      <label >Cast Age</label>
      <input type="number" value="{{$casters->umur}}" name="umur" class="form-control">
    </div>
    <div class="form-group">
        <label >Cast Bio</label>
        <textarea name="bio" class="form-control" id="" cols="30" rows="10">{{$casters->bio}}</textarea>
      </div>
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection
@extends('layout.master')

@section('judul')
Halaman Detail Cast
@endsection
@section('content')

<h1>{{$casters->nama}}</h1>
<p>{{$casters->umur}}</p>
<p>{{$casters->bio}}</p>

<a href="/cast" class="btn btn-secondary btn-sm">Kembali</a>

@endsection
@extends('layout.master')

@section('judul')
New Cast Form 
@endsection

@section('content')
<form action="/cast" method="POST">
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
 
<!-- Create Post Form -->
    @csrf
    <div class="form-group">
      <label >Cast Name</label>
      <input type="text" name="nama" class="form-control">
    </div>
    <div class="form-group">
      <label >Cast Age</label>
      <input type="number" name="umur" class="form-control">
    </div>
    <div class="form-group">
        <label >Cast Bio</label>
        <textarea name="bio" class="form-control" id="" cols="30" rows="10"></textarea>
      </div>
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection
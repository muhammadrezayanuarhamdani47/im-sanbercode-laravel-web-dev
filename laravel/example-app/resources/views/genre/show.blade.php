@extends('layout.master')

@section('judul')
Genre
@endsection
@section('content')

<h1>{{$genres->nama}}</h1>

<div class="row">
@forelse ($genres->listGenre as $item)
<div class="col-3">
    <div class="card">
      <img src="{{asset('image/'.$item->poster)}}" height="400px" class="card-img-top" alt="...">
      <div class="card-body">
        <h2>{{$item->judul}}</h2>
        <h5>{{$item->tahun}}</h6>
        <p class="card-text"> {{Str::limit($item->ringkasan, 80)}}</p>
        <a href="/film/{{$item->id}}" class="btn btn-primary btn-block">Detail</a>
        </div>
      </div>
    </div>
  </div>
@empty
    <h4>Tidak ada film di genre ini</h4>
@endforelse
</div>


<a href="/genre" class="btn btn-secondary btn-sm">Kembali</a>

@endsection
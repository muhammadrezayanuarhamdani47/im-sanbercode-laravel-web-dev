@extends('layout.master')

@section('judul')
Edit Genre 
@endsection

@section('content')
<form action="/genre/{{$genres->id}}" method="POST">
    @method('put')
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
 
<!-- Create Post Form -->
    @csrf
    <div class="form-group">
      <label >Genre Name</label>
      <input type="text" value="{{$genres->nama}}" name="nama" class="form-control">
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection
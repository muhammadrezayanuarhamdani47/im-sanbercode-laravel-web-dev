@extends('layout.master')

@section('judul')
Create Genre
@endsection

@section('content')
<form action="/genre" method="POST">
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
 
<!-- Create Post Form -->
    @csrf
    <div class="form-group">
      <label >Genre Name</label>
      <input type="string" name="nama" class="form-control">
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
@endsection
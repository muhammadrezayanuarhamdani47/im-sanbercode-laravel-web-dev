<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CastController;
use App\Http\Controllers\GenreController;
use App\Http\Controllers\FilmController;
use App\Http\Controllers\CommentsController;





/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', [HomeController::class, 'home']);
Route::get('/register', [AuthController::class, 'register']);
Route::post('/welcome', [AuthController::class, 'welcome']);

Route::get('/table', function(){
    return view ('page.table');
});

Route::get('/data-table', function(){
    return view ('page.data-table');
});

//CRUD Cast
// Create Data Cast
Route::get('/cast/create', [CastController::class, 'create']); //untuk mengarah ke tampilan form tambah cast
Route::post('/cast', [CastController::class, 'store']); //menyimpan data cast ke DB table cast & validation 

//read data cast
Route::get('/cast', [CastController::class, 'index']); //untuk menampilkan semua data cast ke table
Route::get('/cast/{cast_id}', [CastController::class, 'show']); //untuk menampilkan data berdasarkan parameter id

Route::get('/cast/{cast_id}/edit', [CastController::class, 'edit']);//mengarah ke form edit data

Route::put('/cast/{cast_id}', [CastController::class, 'update']); //untuk update data berdasarkan id di DB table Cast

//delete data cast
Route::delete('/cast/{cast_id}', [CastController::class, 'destroy']); // utk delete data di DB table Cast berdasarkan id

Route::middleware(['auth'])->group(function () {
        //CRUD Genre Table
        Route::get('/genre/create', [GenreController::class, 'create']);
        Route::post('/genre', [GenreController::class, 'store']);
        Route::get('/genre', [GenreController::class, 'index']);
        Route::get('/genre/{genre_id}', [GenreController::class, 'show']); 
        Route::get('/genre/{genre_id}/edit', [GenreController::class, 'edit']);
        Route::put('/genre/{genre_id}', [GenreController::class, 'update']); 
        Route::delete('/genre/{genre_id}', [GenreController::class, 'destroy']); 

        //CRUD Film Table
        Route::get('/film/create', [FilmController::class, 'create']);
        Route::post('/film', [FilmController::class, 'store']);
        Route::get('/film/{film_id}/edit', [FilmController::class, 'edit']);
        Route::put('/film/{film_id}', [FilmController::class, 'update']); 
        Route::delete('/film/{film_id}', [FilmController::class, 'destroy']); 

        Route::post("/comments/{film_id}", [CommentsController::class, 'store']);
    });
    





Route::get('/film', [FilmController::class, 'index']);
Route::get('/film/{film_id}', [FilmController::class, 'show']); 
Auth::routes();


